import React from "react";
import styled from "styled-components";

const InputWrapper = styled.div`
  padding: 10px;
  input {
    margin-top: 10px;
    font-size: 1rem;
    background-color: inherit;
    border-radius: 5px;
    padding: 10px;
    display: block;
    border: 1px solid whitesmoke;
    width: 100%;
    &:focus {
      outline: none;
    }
  }
`;

const Input = ({ label, onChange, value }) => (
  <InputWrapper>
    <span className="label">{label}</span>
    <input onChange={onChange} value={value} />
  </InputWrapper>
);

export default Input;
