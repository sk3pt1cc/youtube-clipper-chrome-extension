export default {
    darkest: '#141921',
    dark: '#232b38',
    light:'#374356',
};