import React from "react";
import styled from "styled-components";
import { views } from "../../App";
import colors from "../../colors";

const NavbarWrapper = styled.div`
  padding: 16px;
  background-color: ${colors.dark};
  margin-bottom: 20px;
  border-bottom: 1px solid black;
  display: flex;
  div {
    width: 20%;
  }
  h1,
  h2 {
    font-size: 1rem;
  }

  .title {
    width: 80%;
  }
`;

const Navbar = ({ setCurrentView }) => (
  <NavbarWrapper>
    <div className="title">
      <h1>ClipDrawer</h1>
    </div>
    <div>
      <h2 onClick={() => setCurrentView(views.MAIN)}>Categories</h2>
    </div>
    <div>
      <h2 onClick={() => setCurrentView(views.ADD_VIDEO)}>Add Video</h2>
    </div>
  </NavbarWrapper>
);

export default Navbar;
