import React, { useState } from "react";
import styled from "styled-components";
import qs from "query-string";
import uuid from "uuid";
import moment from "moment";
import Input from "../../styled/Input";
import { views } from "../../App";
import TimePicker from "../TimePicker";

const retrieveCategoryFromList = (categories, title) =>
  categories.find(
    category => category.title.toLowerCase() == title.toLowerCase()
  );

const getVideoObj = (id, start, end, videoTitle) => ({
  id,
  start,
  end,
  title: videoTitle
});

const getOrCreateCategory = (categoryTitle, oldCategories) => {
  const oldCategory = {
    ...oldCategories.find(category => category.title === categoryTitle)
  };
  return oldCategory.id
    ? oldCategory
    : { title: categoryTitle, id: uuid(), videos: [] };
};

const getVideoIdFromUrl = url => {
  const params = qs.parseUrl(url);
  const {
    query: { v }
  } = params;
  return v;
};

const AddVideoFormWrapper = styled.div`
  padding-top: 20px;
  .time-section {
    display: flex;
  }
  h3 {
    margin-bottom: 20px;
  }
  button {
    padding: 5px;
    width: 100px;
    background-color: white;
    color: black;
    border-radius: 500px;
    margin-top: 20px;
    float: right;
    &:disabled {
      color: grey;
      background-color: darkgrey;
    }
  }
`;

const AddVideoForm = ({ categories, setCategories, setCurrentView }) => {
  const [videoTitle, setVideoTitle] = useState("");
  const [url, setUrl] = useState("");
  const [start, setStart] = useState("");
  const [end, setEnd] = useState("");
  const [categoryTitle, setCategoryTitle] = useState("");

  const addToCategory = (newVideoProperties, categoryTitle, oldCategories) => {
    const newCategory = getOrCreateCategory(categoryTitle, oldCategories);
    newCategory.videos.push({
      ...newVideoProperties,
      id: getVideoIdFromUrl(url)
    });

    const updatedCategories = [
      ...oldCategories.filter(
        category => category.title.toLowerCase() !== categoryTitle.toLowerCase()
      ),
      newCategory
    ];

    setCategories(updatedCategories);
    setCurrentView(views.MAIN);
  };

  const formatAndSaveTime = (timeStr, start) => {
    const seconds = moment(timeStr, "HH:mm:ss").diff(
      moment().startOf("day"),
      "seconds"
    );
    if (start) {
      setStart(seconds);
    } else {
      setEnd(seconds);
    }
  };

  const formatAndSaveStartTime = (hours, minutes, seconds) =>
    formatAndSaveTime(`${hours}:${minutes}:${seconds}`, true);
  const formatAndSaveEndTime = (hours, minutes, seconds) =>
    formatAndSaveTime(`${hours}:${minutes}:${seconds}`, false);

  return (
    <AddVideoFormWrapper>
      <h3>Add New Video</h3>
      <Input
        label="Category title*"
        onChange={e => setCategoryTitle(e.target.value)}
        value={categoryTitle}
      />
      <Input
        label="Video title*"
        onChange={e => setVideoTitle(e.target.value)}
        value={videoTitle}
      />
      <Input
        label="Video url*"
        onChange={e => setUrl(e.target.value)}
        value={url}
      />
      <div className="time-section">
        <TimePicker label="Start time" setTime={formatAndSaveStartTime} />
        <TimePicker label="End time" setTime={formatAndSaveEndTime} />
      </div>
      <button
        disabled={!videoTitle || !url || !categoryTitle}
        onClick={e =>
          addToCategory({ url, start, end, title: videoTitle }, categoryTitle, [
            ...categories
          ])
        }
      >
        Submit
      </button>
    </AddVideoFormWrapper>
  );
};

export default AddVideoForm;
