import React from "react";

const Select = ({ limit, onChange, value, unit }) => (
  <select onChange={e => onChange(e.target.value)} value={value}>
    {[...Array(limit)].map((val, idx) => {
      const value = idx - 10 < 0 ? `0${idx}` : `${idx}`;
      return (
        <option value={value}>
          {value}{unit}
        </option>
      );
    })}
  </select>
);

export default Select;
