import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import Video from "./Video";

const VideoListWrapper = styled.div`
  .warning {
    padding: 16px;
    background-color: #ff6666;
    text-align: center;
  }
  margin-bottom: 20px;
`;

const VideoList = ({ videos, parentCategory, setCategories, categories }) => {

  const deleteVideo = (oldCategory, videoId, oldCategories) => {
    const newCategory = {
      ...oldCategory,
      videos: oldCategory.videos.filter(video => video.id !== videoId)
    };
    setCategories(
      oldCategories.map(category =>
        category.title === oldCategory.title ? newCategory : category
      )
    );
  };

  return (
    <VideoListWrapper>
      {videos.map(video => (
        <Video
          {...video}
          key={video.id}
          parentCategory={parentCategory}
          deleteVideo={() => deleteVideo({ ...parentCategory }, video.id, [ ...categories ])}
        />
      ))}
    </VideoListWrapper>
  );
};
VideoList.defaultProps = {
  videos: []
};

VideoList.propTypes = {
  videos: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      start: PropTypes.number,
      end: PropTypes.number,
      title: PropTypes.string
    })
  )
};

export default VideoList;
