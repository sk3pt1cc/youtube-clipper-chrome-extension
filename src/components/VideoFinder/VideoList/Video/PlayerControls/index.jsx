import React from "react";
import styled from "styled-components";
import { FaPlayCircle, FaPauseCircle, FaUndo } from "react-icons/fa";
import PlaybackPositionControl from "./PlaybackPositionControl";
import VolumeControls from "./VolumeControls";

const PlayerControlsWrapper = styled.div`
  .player-slider {
    width: 100%;
    padding: 10px;
  }
  .player-controls {
    section {
      display: inline-block;
      svg {
        cursor: pointer;
        color: whitesmoke;
        margin: 0 10px;
      }
    }
    .player-volume {
      float: right;
    }
  }
`;

const PlayerControls = props => {
  const {
    startPlayback,
    isPlaying,
    pausePlayback,
    restartVideo,
    setPlaybackPosition,
    playbackPosition,
    isMuted,
    setMute,
    setVolume,
    volume,
  } = props;
  return (
    <PlayerControlsWrapper>
      <section className="player-slider">
        <PlaybackPositionControl
          setPlaybackPosition={setPlaybackPosition}
          playbackPosition={playbackPosition}
        />
      </section>
      <section className="player-controls">
        <section className="player-playback">
          <span onClick={restartVideo}>
            <FaUndo />
          </span>
          {isPlaying ? (
            <span onClick={pausePlayback}>
              <FaPauseCircle />
            </span>
          ) : (
            <span onClick={startPlayback}>
              <FaPlayCircle />
            </span>
          )}
        </section>
        <section className="player-volume">
          <VolumeControls isMuted={isMuted} setMute={setMute} setVolume={setVolume} volume={volume} />
        </section>
      </section>
    </PlayerControlsWrapper>
  );
};

export default PlayerControls;
