import React, { useState } from "react";
import styled from "styled-components";
import { FaVolumeMute, FaVolumeUp } from "react-icons/fa";
import VolumeOrb from "../../../../../../svg/VolumeOrb";
import { volumeLevels } from '../../'; 

const VolumeControlsWrapper = styled.div`
  padding: 0;
  display: flex;
`;

const VolumeControls = ({ isMuted, setMute, setVolume, volume }) => (
    <VolumeControlsWrapper>
      <section className="volume-icons">
        <span onClick={setMute}>
          {isMuted ? <FaVolumeMute /> : <FaVolumeUp />}
        </span>
      </section>
      <section className="volume-orbs">
        {volumeLevels.map(level => (
          <VolumeOrb
            onClick={() => setVolume(level)}
            color={volume >= level ? "#70ff2d" : "white"}
          />
        ))}
      </section>
    </VolumeControlsWrapper>
);

export default VolumeControls;
