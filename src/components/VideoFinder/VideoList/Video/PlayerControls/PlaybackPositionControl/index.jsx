import React from "react";
import styled from "styled-components";
import Slider, { Range } from "rc-slider";
import "rc-slider/assets/index.css";

const PlaybackPositionControlWrapper = styled.div``;

const PlaybackPositionControl = ({ setPlaybackPosition, playbackPosition }) => (
  <PlaybackPositionControlWrapper>
    <Slider
      min={0}
      max={100}
      value={playbackPosition}
      onChange={setPlaybackPosition}
    />
  </PlaybackPositionControlWrapper>
);

export default PlaybackPositionControl;
