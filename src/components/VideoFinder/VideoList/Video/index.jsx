import React from "react";
import styled from "styled-components";
import Player from "react-player";
import moment from "moment";
import { MdDelete } from "react-icons/md";
import { TiTick, TiDelete } from "react-icons/ti";
import PlayerControls from "./PlayerControls";
import colors from "../../../../colors";

export const volumeLevels = [0.2, 0.4, 0.6, 0.8, 1];

const wrapperWidth = 550;
const spaceAroundVideo = 32;
const videoWidth = wrapperWidth - spaceAroundVideo;

const getPositionInSeconds = (duration, val) =>
  Math.floor((duration / 100) * val);

const VideoWrapper = styled.div`
  width: ${wrapperWidth}px;
  padding: ${spaceAroundVideo / 2}px;
  .video-header {
    padding: 16px;
    display: inline-block;
    border-top-left-radius: 5px;
    border-top-right-radius: 5px;
    background-color: ${colors.darkest};
    .video-title {
      display: inline-block;
      h1 {
        font-size: 1rem;
        display: inline-block;
        margin-right: 20px;
      }
      span {
        font-size: 0.7rem;
        background-color: #182533;
        padding: 10px;
      }
    }
    .confirm-delete-msg {
        font-size: 0.8rem;
        margin-top: 20px;
        span {
          margin-left: 15px;
          cursor: pointer;
        }
      }
    .video-delete {
      display: inline-block;
      margin-left: 20px;
      cursor: pointer;
      &:hover {
        color: red;
      }
    }
  }
  .player-content {
    background-color: ${colors.darkest};
    .video {
      width: ${videoWidth}px;
      height: ${(videoWidth / 16) * 9}px;
    }
    .player-controls {
      padding: 5px;
    }
  }
`;

class Video extends React.Component {
  constructor() {
    super();
    this.videoPlayer = React.createRef();
    this.state = {
      playing: false,
      playbackPosition: 0,
      isMuted: false,
      volume: volumeLevels[2],
      confirmDelete: false
    };
  }

  seekTo = val => {
    const { start, end } = this.props;
    this.videoPlayer.seekTo(val, "seconds");
  };

  startPlayback = () => {
    this.videoPlayer.getInternalPlayer().playVideo();
  };

  pausePlayback = () => {
    this.videoPlayer.getInternalPlayer().pauseVideo();
  };

  setMute = () => {
    this.setState({ isMuted: !this.state.isMuted });
  };

  /**
   * 'val' param represents percentage value
   * of clicked position on slider component
   *
   * We add the 'start' prop to the position in seconds because we
   * are just simulating a clip, but still working with the full video file.
   * We need to get the number of seconds played relative to the
   * start of the clip, not the video. I hope this makes sense. But it probably doesn't.
   */
  setPlaybackPosition = (duration, val) => {
    this.setState({ playbackPosition: val });

    const positionInSeconds = getPositionInSeconds(duration, val);
    this.seekTo(positionInSeconds + this.props.start);
    this.startPlayback();
  };

  /**
   * Same sort of thing here - but subtracting 'start' instead of adding it -
   * as described above setPlaybackPosition method.
   */
  onPlaying = (duration, start, event) => {
    if (this.state.playing) {
      const { playedSeconds } = event;
      const playbackPosition = ((playedSeconds - start) / duration) * 100;
      this.setState({ playbackPosition });
    }
  };

  setVolume = volume => {
    this.setState({ volume });
  };

  setPlaying = playing => {
    this.setState({ playing });
  };

  restartVideo = duration => {
    this.setPlaybackPosition(duration, 0);
    this.pausePlayback();
  };

  setConfirmDelete = confirmDelete => {
    this.setState({ confirmDelete });
  };

  render() {
    const { id, start, end, title, deleteVideo } = this.props;
    const {
      playing,
      playbackPosition,
      isMuted,
      volume,
      confirmDelete
    } = this.state;
    const fullUrl = `https://www.youtube.com/embed/${id}&start=${start}&end=${end}`;
    const duration = end - start;

    const positionInSeconds = getPositionInSeconds(duration, playbackPosition);
    const formattedTime = moment("2015-01-01")
      .startOf("day")
      .seconds(positionInSeconds)
      .format("HH:mm:ss");

    return (
      <VideoWrapper>
        <div className="video-header">
          <div className="video-title">
            <h1>{title}</h1>
            <span>{formattedTime}</span>
          </div>
          <div className="video-delete">
            {!confirmDelete && (
              <span onClick={() => this.setConfirmDelete(true)}>
                <MdDelete />
              </span>
            )}
          </div>
          {confirmDelete && (
            <div className="confirm-delete-msg">
              Are you sure? This can't be undone.
              <span onClick={deleteVideo}>
                <TiTick />
              </span>
              <span onClick={() => this.setConfirmDelete(false)}>
                {/*
                This is a wee bit confusing, but the 'TiDelete' component is actually a cross,
                which I'm using to represent 'No, I don't want to delete'. So this actually
                cancels the deletion.
              */}
                <TiDelete />
              </span>
            </div>
          )}
        </div>
        <div className="player-content">
          <div className="video">
            <Player
              url={fullUrl}
              ref={ref => (this.videoPlayer = ref)}
              onProgress={e => this.onPlaying(duration, start, e)}
              width="100%"
              height="100%"
              volume={volume}
              muted={isMuted}
              onStart={() => this.setPlaying(true)}
              onPlay={() => this.setPlaying(true)}
              onPause={() => this.setPlaying(false)}
              onEnded={() => this.setPlaying(false)}
            />
          </div>
          <div className="player-controls">
            <PlayerControls
              startPlayback={this.startPlayback}
              pausePlayback={this.pausePlayback}
              setVolume={this.setVolume}
              volume={volume}
              restartVideo={() => this.restartVideo(duration)}
              setPlaybackPosition={val =>
                this.setPlaybackPosition(duration, val)
              }
              isPlaying={playing}
              playbackPosition={playbackPosition}
              setMute={this.setMute}
              isMuted={isMuted}
            />
          </div>
        </div>
      </VideoWrapper>
    );
  }
}

export default Video;
