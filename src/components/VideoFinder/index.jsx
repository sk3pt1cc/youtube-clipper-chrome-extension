import React, { useState } from "react";
import styled from "styled-components";
import colors from "../../colors";
import Input from "../../styled/Input";
import VideoList from "./VideoList";
import { FaAngleDown, FaAngleUp } from "react-icons/fa";

const VideoFinderWrapper = styled.div`
    h2 {
        font-weight: normal;
        font-size: 1rem;
        margin-bottom: 10px;
    }
    .dark-panel {
        width: 100%;
        padding: 16px;
        background-color: ${colors.dark}
        margin-bottom: 20px
        border-radius: 10px;
    }
    .category-list {
        .category-preview {
            .category-preview-header {
                display: flex;
                .expand-controls {
                    cursor: pointer;
                }
                h3 {
                    margin-bottom: 10px;
                    .video-count {
                        margin-left: 10px;
                        color: lightgrey;
                        font-size: 13px;
                        font-weight: normal;
                    }
                }
                .header {
                    width: 97%;
                }
            }
        }
    }
`;

const VideoFinder = ({ categories, setCategories }) => {
  const [filterTerm, setFilterTerm] = useState("");
  const [selectedCategory, setSelectedCategory] = useState("");

  const updateSelectedCategory = id => {
    if (selectedCategory === id) {
      setSelectedCategory("");
    } else {
      setSelectedCategory(id);
    }
  };

  const filteredCategories = categories.filter(category =>
    category.title.toLowerCase().includes(filterTerm.toLowerCase())
  );

  return (
    <VideoFinderWrapper>
      <section className="dark-panel category-filter">
        <Input
          label="Start typing to filter categories"
          onChange={e => setFilterTerm(e.target.value)}
          value={filterTerm}
        />
      </section>
      <section className="category-list">
        {filteredCategories.map(filteredCategory => (
          <div className="dark-panel category-preview">
            <div className="category-preview-header">
              <div className="header">
                <h3>
                  {filteredCategory.title}{" "}
                  <span className="video-count">
                    {filteredCategory.videos.length} video(s)
                  </span>
                </h3>
              </div>
              { filteredCategory.videos.length > 0 && (<div
                className="expand-controls"
                onClick={() => updateSelectedCategory(filteredCategory.id)}
              >
                {filteredCategory.id === selectedCategory ? (
                  <FaAngleUp />
                ) : (
                  <FaAngleDown />
                )}
              </div>)}
            </div>
            {filteredCategory.id === selectedCategory && filteredCategory.videos.length > 0 && (
              <VideoList
                categories={categories}
                videos={filteredCategory.videos}
                parentCategory={filteredCategory}
                setCategories={setCategories}
              />
            )}
          </div>
        ))}
      </section>
    </VideoFinderWrapper>
  );
};
export default VideoFinder;
