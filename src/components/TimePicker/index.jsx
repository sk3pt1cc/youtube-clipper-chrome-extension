import React, { useState, useEffect } from "react";
import styled from "styled-components";
import moment from 'moment';
import Select from "../Select";

const TimePickerWrapper = styled.div`
    margin: 10px;
    margin-bottom: 30px;
    .label {
        margin-bottom: 10px;
    }
    select {
        outline: none;
        border: none;
        width: 80px;
        border: 1px solid white;
        background-color: inherit;
    }
`;

const TimePicker = ({ label, setTime }) => {
  const [hours, setHours] = useState('00');
  const [minutes, setMinutes] = useState('00');
  const [seconds, setSeconds] = useState('00');

  useEffect(() => {
    setTime(hours, minutes, seconds);
  }, [hours, minutes, seconds])

  return (
    <TimePickerWrapper>
      <div className="label">
        <p>{label}</p>
      </div>
      <span className="hours">
        <Select limit={10} onChange={setHours} value={hours} unit="h" />
      </span>
      :
      <span className="minutes">
        <Select limit={60} onChange={setMinutes} value={minutes} unit="m" />
      </span>
      :
      <span className="seconds">
        <Select limit={60} onChange={setSeconds} value={seconds} unit="s" />
      </span>
    </TimePickerWrapper>
  );
};

export default TimePicker;
