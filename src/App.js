/* global chrome */

import React, { useState, useEffect } from "react";
import styled, { createGlobalStyle } from "styled-components";
import AddVideoForm from "./components/AddVideoForm";
import Navbar from "./components/Navbar";
import VideoFinder from "./components/VideoFinder";
import colors from "./colors";

export const views = { MAIN: 1, ADD_VIDEO: 2 };

const GlobalStyle = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    color: white;
  }
  body {
    font-family: Arial, Helvetica, sans-serif;
  }
`;

const AppWrapper = styled.div`
  display: inline-block;
  height: 600px;
  width: 600px;
  border-bottom: 1px solid black;
  background-color: ${colors.light};
  overflow-y: scroll;
  .content {
    padding: 16px;
  }
`;

const App = () => {
  const [categories, setCategories] = useState([]);
  const [currentView, setCurrentView] = useState(views.ADD_VIDEO);

  useEffect(() => {
    if (chrome.storage) {
      chrome.storage.sync.get(["categories"], response => {
        setCategories(response.categories || []);
        if (response.categories) {
          if (response.categories.length) {
            setCurrentView(views.MAIN);
          }
        }
      });
    }
  }, []);

  const updateCategories = updatedCategories => {
    setCategories(updatedCategories);
    if (chrome.storage) {
      chrome.storage.sync.set({ categories: updatedCategories });
    }
  };

  return (
    <AppWrapper>
      <GlobalStyle />
      <Navbar setCurrentView={setCurrentView} />
      <section className="content">
        {currentView === views.MAIN ? (
          <VideoFinder
            categories={categories}
            setCategories={updateCategories}
          />
        ) : (
          <AddVideoForm
            categories={categories}
            setCategories={updateCategories}
            setCurrentView={setCurrentView}
          />
        )}
      </section>
    </AppWrapper>
  );
};

export default App;
