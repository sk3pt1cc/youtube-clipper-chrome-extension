import React from "react";

const VolumeBar = ({ color, onClick }) => (
  <svg
    width="12px"
    height="12px"
    viewBox="0 0 7 7"
    version="1.1"
    onClick={onClick}
  >
    <g
      id="Page-1"
      stroke="none"
      strokeWidth="1"
      fill="none"
      fillRule="evenodd"
    >
      <circle
        id="Oval"
        stroke="#979797"
        fill={color}
        cx="3.5"
        cy="3.5"
        r="2.5"
      />
    </g>
  </svg>
);

export default VolumeBar;
